import Graphics.Element (..)
import Graphics.Collage (..)
import Signal
import Signal (Signal)
import Text
import Time
import Window
import Keyboard
import Mouse
import List as L
import Random as R
import Color (..)

{-- Part 1: Model the user input ----------------------------------------------

What information do you need to represent all relevant user input?

Task: Redefine `UserInput` to include all of the information you need.
      Redefine `userInput` to be a signal that correctly models the user
      input as described by `UserInput`.

------------------------------------------------------------------------------}

{--
type UserInput = KeyboardInput { x : Int, y : Int }
  | MouseInput (Int, Int)
  --}
type alias KeyInput = { x : Int, y : Int }
type alias MouseInput = (Int, Int)
type alias MouseDownInput = Bool
type alias UserInput = { keys : KeyInput, mouse : MouseInput, mouseDown : MouseDownInput }


combineInputs : KeyInput -> MouseInput -> MouseDownInput -> UserInput
combineInputs keys mouse mouseDown = { keys = keys, mouse = mouse, mouseDown = mouseDown }


userInput : Signal UserInput
userInput = Signal.map3 combineInputs Keyboard.wasd Mouse.position Mouse.isDown


type alias Input =
    { timeDelta : Float
    , userInput : UserInput
    }



{-- Part 2: Model the game ----------------------------------------------------

What information do you need to represent the entire game?

Tasks: Redefine `GameState` to represent your particular game.
       Redefine `defaultGame` to represent your initial game state.

For example, if you want to represent many objects that just have a position,
your GameState might just be a list of coordinates and your default game might
be an empty list (no objects at the start):

    type GameState = { objects : [(Float,Float)] }
    defaultGame = { objects = [] }

------------------------------------------------------------------------------}

-- Game configuration constants
bulletSpeedConstant = 10
bulletWidth = 10
bulletHeight = 3
albumMovementSpeed = 3
maxActiveAlbums = 3
playerSpeedConstant = 5
firingCooldownFrames = 10

albums : List String
albums = [
  "http://img00.cdn2-rdio.com/album/4/5/e/00000000000f2e54/1/square-400.jpg",
  "http://img02.cdn2-rdio.com/album/5/6/a/00000000000e7a65/1/square-400.jpg",
  "http://rdio3img-a.akamaihd.net/album/c/f/5/00000000000e65fc/5/square-400.jpg",
  "http://img00.cdn2-rdio.com/album/0/1/4/00000000000e1410/1/square-400.jpg",
  "http://img02.cdn2-rdio.com/album/8/c/8/00000000000df8c8/square-400.jpg",
  "http://img00.cdn2-rdio.com/album/b/9/2/000000000001629b/4/square-400.jpg",
  "http://img02.cdn2-rdio.com/album/7/0/9/00000000000d8907/1/square-400.jpg",
  "http://rdio1img-a.akamaihd.net/album/4/a/a/00000000000cdaa4/1/square-400.jpg",
  "http://img00.cdn2-rdio.com/album/7/1/4/00000000000c5417/1/square-400.jpg",
  "http://img00.cdn2-rdio.com/album/1/f/d/00000000000a7df1/1/square-400.jpg",
  "http://rdio1img-a.akamaihd.net/album/6/a/e/00000000000a4ea6/3/square-400.jpg",
  "http://rdio3img-a.akamaihd.net/album/d/b/f/0000000000522fbd/1/square-400.jpg",
  "http://rdio3img-a.akamaihd.net/album/b/b/f/0000000000522fbb/1/square-400.jpg",
  "http://rdio3img-a.akamaihd.net/album/4/b/f/0000000000522fb4/1/square-400.jpg",
  "http://rdio3img-a.akamaihd.net/album/8/8/f/0000000000522f88/1/square-400.jpg",
  "http://rdio1img-a.akamaihd.net/album/5/8/f/0000000000522f85/1/square-400.jpg",
  "http://rdio1img-a.akamaihd.net/album/9/7/f/0000000000522f79/1/square-400.jpg",
  "http://rdio1img-a.akamaihd.net/album/7/7/f/0000000000522f77/1/square-400.jpg",
  "http://rdio3img-a.akamaihd.net/album/0/7/f/0000000000522f70/1/square-400.jpg",
  "http://rdio1img-a.akamaihd.net/album/c/6/f/0000000000522f6c/1/square-400.jpg"]

type GameStatus =
  Playing |
  Victorious |
  Defeated

type alias Bullet = {
  xVel : Float,
  yVel : Float,
  position : (Float, Float)
}

type alias ActiveAlbum = {
  position : (Float, Float),
  src : String
}

type alias GameState = {
  gameStatus : GameStatus,
  playerPosition : (Float,Float),
  mousePosition : (Float, Float),  
  bulletPositions : List Bullet,
  activeFiringCooldownFrames : Int,
  waitingAlbums : List String,
  activeAlbums : List ActiveAlbum,
  seed : R.Seed
}


defaultGame : GameState
defaultGame = { 
  gameStatus = Playing,
  playerPosition = (0, 0),
  mousePosition = (0, 0),
  bulletPositions = [],
  activeFiringCooldownFrames = 0,
  waitingAlbums = albums,
  activeAlbums = [],
  seed = R.initialSeed 0 }



{-- Part 3: Update the game ---------------------------------------------------

How does the game step from one state to another based on user input?

Task: redefine `stepGame` to use the UserInput and GameState
      you defined in parts 1 and 2. Maybe use some helper functions
      to break up the work, stepping smaller parts of the game.

------------------------------------------------------------------------------}

-- TODO: window dimensions instead of magic nums
isBulletInBounds : Bullet -> Bool
isBulletInBounds bullet =
  let ypos = snd bullet.position
  in (ypos <= 400 && ypos >= -400)

-- Given a bullet, returns a new bullet with position updated from its velocity.
moveBullet : Bullet -> Bullet
moveBullet b = 
  let newPosition = (b.xVel + (fst b.position), b.yVel + (snd b.position))
  in { xVel = b.xVel, yVel = b.yVel, position = newPosition }

-- Given the player position, and the mouse position, returns a new bullet
-- at the player's position, with velocity moving it towards the mouse position.
makeNewBullet : (Float, Float) -> (Float, Float) -> Bullet
makeNewBullet (px, py) (mx, my) =
  let goingUp = py < my
      goingRight = px < mx
      yVel = if goingUp then 1 else -1
      xVel = if goingRight then 1 else -1
      yDisp = abs(py - my)
      xDisp = abs(px - mx)
      adjustedYVel = bulletSpeedConstant * (if yDisp > xDisp then yVel else yVel * (yDisp / xDisp))
      adjustedXVel = bulletSpeedConstant * (if xDisp > yDisp then xVel else xVel * (xDisp / yDisp))
  in
     { xVel = adjustedXVel, yVel = adjustedYVel, position = (px, py) }

-- Given trigger status and game state, returns a tuple with True if a new
-- bullet was fired, and a list of new bullet positions, with each one moved
-- according to its velocity.
getNewBulletPositions : Bool ->  GameState -> (Bool, List Bullet)
getNewBulletPositions triggerDown gameState =
  let inBoundsBullets = L.filter isBulletInBounds gameState.bulletPositions
      movedBullets = L.map moveBullet inBoundsBullets
      newBullet = makeNewBullet gameState.playerPosition (getAdjustedMousePosition gameState.mousePosition)
  in
    if triggerDown && (gameState.activeFiringCooldownFrames == 0)
       then (True, newBullet :: movedBullets)
       else (False, movedBullets)

moveActiveAlbum : (Float, Float) -> ActiveAlbum -> ActiveAlbum
moveActiveAlbum (px, py) aa =
  let (aaX, aaY) = aa.position
      nextAAX = if | aaX == px -> aaX
                   | aaX > px -> aaX - albumMovementSpeed
                   | otherwise -> aaX + albumMovementSpeed
      nextAAY = if | aaY == py -> aaY
                   | aaY > py -> aaY - albumMovementSpeed
                   | otherwise -> aaY + albumMovementSpeed
  in { position = (nextAAX, nextAAY), src = aa.src }


isOnPlayer : (Float, Float) -> ActiveAlbum -> Bool
isOnPlayer (px, py) aa =
  (abs (px - (fst aa.position))) <= 50 && (abs (py - (snd aa.position))) <= 50


isDestroyedByBullet : ActiveAlbum -> Bullet -> Bool
isDestroyedByBullet aa bullet =
  (abs ((fst aa.position) - (fst bullet.position)) < 20) &&
  (abs ((snd aa.position) - (snd bullet.position)) < 20)


-- TODO: Don't hard-code magic nums
xCoordGenerator = R.generate (R.float -1000 1000)
yCoordGenerator = R.generate (R.float -400 400)

-- Get a random position that's not too close to the player
getRandomPosition : (Float, Float) -> R.Seed -> (Float, Float, R.Seed)
getRandomPosition (px, py) seed = 
  let (randomX, seed') = xCoordGenerator seed
      (randomY, seed'') = yCoordGenerator seed'
  in (randomX, randomY, seed'')

makeNewActive : (Float, Float) -> R.Seed -> String -> (R.Seed, ActiveAlbum)
makeNewActive playerPos seed src =
  let (newX, newY, seed') = getRandomPosition playerPos seed
  in (seed', { src = src, position = (newX, newY) })


type alias AlbumsRecord = { waiting : List String, active : List ActiveAlbum }

newAlbumFolder : (Float, Float) -> String -> (R.Seed, List ActiveAlbum) -> (R.Seed, List ActiveAlbum)
newAlbumFolder playerPos src (seed, albums) =
  let (seed', newActive) = makeNewActive playerPos seed src
  in (seed', newActive :: albums)


getNewActiveAlbums : List Bullet -> (Float, Float) -> R.Seed -> AlbumsRecord -> (R.Seed, AlbumsRecord)
getNewActiveAlbums bullets playerPosition seed {waiting,active} =
  let movedActives = L.map (moveActiveAlbum playerPosition) active
      albumsNotDestroyed = L.filter (\al -> not (L.any (isDestroyedByBullet al) bullets)) movedActives
      newActivesRequired = max (maxActiveAlbums - (L.length albumsNotDestroyed)) 0
      newSrcs = L.take newActivesRequired waiting
      (seed', newActives) = L.foldl (newAlbumFolder playerPosition) (seed, []) newSrcs
      withNewActives = albumsNotDestroyed ++ newActives
      newWaiting = L.drop newActivesRequired waiting
  in (seed', { waiting = newWaiting, active = withNewActives })


stepGame : Input -> GameState -> GameState
stepGame {timeDelta,userInput} gameState =
  let keyInput = userInput.keys
      newX = playerSpeedConstant * (toFloat keyInput.x) + (fst gameState.playerPosition)
      newY = playerSpeedConstant * (toFloat keyInput.y) + (snd gameState.playerPosition)
      mouseInput = userInput.mouse
      newMouseX = toFloat (fst mouseInput)
      newMouseY = toFloat (snd mouseInput)
      (fired, newBulletPositions) = getNewBulletPositions userInput.mouseDown gameState
      nextFramesToWait = if fired then firingCooldownFrames else max (gameState.activeFiringCooldownFrames - 1) 0
      (seed', {waiting,active}) = getNewActiveAlbums newBulletPositions gameState.playerPosition gameState.seed { waiting = gameState.waitingAlbums, active = gameState.activeAlbums }
      nextGameStatus = case gameState.gameStatus of
        Defeated -> Defeated
        Victorious -> Victorious
        Playing -> if | L.any (isOnPlayer (newX, newY)) active -> Defeated
                      | ((L.length waiting == 0) && (L.length active == 0)) -> Victorious
                      | otherwise -> Playing
  in
     { gameStatus = nextGameStatus,
     playerPosition = (newX, newY),
     mousePosition = (newMouseX, newMouseY),
     bulletPositions = newBulletPositions,
     activeFiringCooldownFrames = nextFramesToWait,
     waitingAlbums = waiting,
     activeAlbums = active, seed = seed' }



{-- Part 4: Display the game --------------------------------------------------

How should the GameState be displayed to the user?

Task: redefine `display` to use the GameState you defined in part 2.

------------------------------------------------------------------------------}

getAdjustedMousePosition : (Float, Float) -> (Float, Float)
getAdjustedMousePosition mp = 
    let mouseX =  (fst mp) - (toFloat 1440 / 2)
        mouseY = (toFloat 800 / 2) - (snd mp)
    in
       (mouseX, mouseY)


displayAlbum : ActiveAlbum -> Form
displayAlbum al = move al.position (toForm (image 50 50 al.src))


displayBullet : Bullet -> Form
displayBullet b =
  let angle = atan2 b.yVel b.xVel
  in move b.position (rotate angle (filled (rgb 255 51 51) (rect bulletWidth bulletHeight)))


displayPlayer : (Float, Float) -> (Float, Float) -> Form
displayPlayer (px, py) (mx, my) =
  let translatedMx = mx - px
      translatedMy = my - py
      angle = (atan2 translatedMy translatedMx) - (degrees 90)
  in move (px, py) (rotate angle (toForm (image 30 30 "images/Starfighter.png")))


displayNotDefeated : (Int,Int) -> GameState -> Element
displayNotDefeated (w,h) gameState =
    let adjustedMousePosition = getAdjustedMousePosition gameState.mousePosition
        playerForm = displayPlayer gameState.playerPosition adjustedMousePosition
        bulletForms = L.map displayBullet gameState.bulletPositions
        albumForms = L.map displayAlbum gameState.activeAlbums
        victoriousMessage = move (0, 0) (toForm (Text.plainText "Victory!"))
        penultimateForms = [playerForm] ++ bulletForms ++ albumForms
        finalForms = case gameState.gameStatus of
          Victorious -> victoriousMessage :: penultimateForms
          _ -> penultimateForms
    in
      collage w h finalForms


displayDefeated : (Int,Int) -> GameState -> Element
displayDefeated (w,h) gameState =
  collage w h [move gameState.playerPosition (toForm (Text.plainText "Game over."))]


display : (Int,Int) -> GameState -> Element
display dimensions gameState = case gameState.gameStatus of
  Defeated -> displayDefeated dimensions gameState
  _ -> displayNotDefeated dimensions gameState


{-- That's all folks! ---------------------------------------------------------

The following code puts it all together and shows it on screen.

------------------------------------------------------------------------------}

delta : Signal Float
delta =
    Time.fps 60


input : Signal Input
input =
    Signal.sampleOn delta (Signal.map2 Input delta userInput)


gameState : Signal GameState
gameState =
    Signal.foldp stepGame defaultGame input


main : Signal Element
main =
    Signal.map2 display Window.dimensions gameState
